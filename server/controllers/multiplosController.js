/** @format */

exports.index = (req, res) => {
    let valores = req.value;
    const divisores = [3, 5];
    const textosAray = ["Music", "TI"];
    let numero;

    valores.forEach((element) => {
        numero = element;
        divisores.forEach((divisor, index) => {
            const texto = textosAray[index];
            const index_numero = valores.indexOf(numero);

            if (numero % divisor == 0) {
                valores[index_numero] = texto;
            }
        });
    });

    return res.status(200).render("./../../views/index", {
        multiplos: valores,
    });
};